﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YesDev.Animator
{
	public class KeepBool : StateMachineBehaviour
	{
		public string boolName;
		public bool status;
		public bool resetOnExit = true;

		// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
		override public void OnStateUpdate(UnityEngine.Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			animator.SetBool(boolName, status); 
		}

		public override void OnStateExit(UnityEngine.Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (resetOnExit)
				animator.SetBool(boolName, !status);
		}
	}
}
