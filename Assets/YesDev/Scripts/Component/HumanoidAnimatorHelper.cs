﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YesDev.Component
{
	public class HumanoidAnimatorHelper : MonoBehaviour
	{
		[SerializeField]
		[Range(0,1)]
		protected float vert;

		[SerializeField]
		[Range(-1,1)]
		protected float horz;

		[SerializeField]
		protected bool playAnim;

		[SerializeField]
		protected bool enableRootMotion;

		protected UnityEngine.Animator anim;

		[SerializeField]
		protected string[] punchAttackAnimNames;

		[SerializeField]
		protected string[] kickAttackAnimNames;

		[SerializeField]
		protected bool altAttack;

		[SerializeField]
		protected bool useItem;

		[SerializeField]
		protected bool interacting;

		[SerializeField]
		protected bool lockOn;

		private void Start()
		{
			anim = GetComponent<UnityEngine.Animator>();
		}

		private void Update()
		{
			enableRootMotion = !anim.GetBool("CanMove");
			anim.applyRootMotion = enableRootMotion;

			interacting = anim.GetBool("Interacting");

			if (!lockOn)
			{
				horz = 0;
				vert = Mathf.Clamp01(vert);
			}

			anim.SetBool("LockOn", lockOn);

			if (enableRootMotion) return;

			if (useItem)
			{
				anim.Play("UseItem");
				useItem = false;
			}

			if (interacting)
			{
				playAnim = false;
				vert = Mathf.Clamp(vert, 0, 0.5f);
			}

			if (playAnim && !interacting)
			{
				string targetAnim;
				if (altAttack)
				{
					targetAnim = kickAttackAnimNames[Random.Range(0, kickAttackAnimNames.Length)];
				}
				else
				{
					targetAnim = punchAttackAnimNames[Random.Range(0, punchAttackAnimNames.Length)];
				}
				vert = 0;
				anim.CrossFade(targetAnim, 0.2f);
				//anim.SetBool("CanMove",false);
				//enableRootMotion = true;
				playAnim = false;
			}

			anim.SetFloat("Vertical", vert);
			anim.SetFloat("Horizontal", horz);
		}
	}
}

