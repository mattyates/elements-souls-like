﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YesDev.Controller
{
	public class CameraManager : MonoBehaviour
	{
		public static CameraManager instance;

		[SerializeField]
		protected bool _lockOn;

		[SerializeField]
		protected float _followSpeed = 9;

		[SerializeField]
		protected float _mouseSpeed = 2;

		[SerializeField]
		protected float _controllerSpeed = 7;

		[SerializeField]
		protected Transform _followTarget;

		[HideInInspector]
		public Transform pivot;

		[HideInInspector]
		public Transform camTransform;

		protected float _turnSmooth = 0.1f;
		
		[SerializeField]
		protected float _minAngle = -15f;

		[SerializeField]
		protected float _maxAngle = 35f;

		protected float _smoothX;
		protected float _smoothY;
		protected float _smoothXVeloc;
		protected float _smoothYVeloc;

		[SerializeField]
		protected float _lookAngle;

		[SerializeField]
		protected float _tiltAngle;

		private void Awake()
		{
			instance = this;
		}

		public void Init(Transform t)
		{
			_followTarget = t;

			camTransform = Camera.main.transform;
			pivot = camTransform.parent;
		}

		public void Tick(float d)
		{
			float h = Input.GetAxis("Mouse X");
			float v = Input.GetAxis("Mouse Y");

			float c_h = Input.GetAxis("RightAxis X");
			float c_v = Input.GetAxis("RightAxis Y") * -1;

			float targetSpeed = _mouseSpeed;

			if (c_h != 0 || c_v != 0)
			{
				h = c_h;
				v = c_v;
				targetSpeed = _controllerSpeed;
			}

			FollowTarget(d);
			HandleRotations(d, v, h, targetSpeed);
		}

		protected void FollowTarget(float d)
		{
			float speed = d * _followSpeed;
			Vector3 targetPosition = Vector3.Lerp(transform.position, _followTarget.position, speed);
			transform.position = targetPosition;
		}

		protected void HandleRotations(float d, float v, float h, float targetSpeed)
		{
			if (_turnSmooth > 0)
			{
				_smoothX = Mathf.SmoothDamp(_smoothX, h, ref _smoothXVeloc, _turnSmooth);
				_smoothY = Mathf.SmoothDamp(_smoothY, v, ref _smoothYVeloc, _turnSmooth);
			}
			else
			{
				_smoothX = h;
				_smoothY = v;
			}

			if (_lockOn)
			{

			}

			_lookAngle += _smoothX * targetSpeed;
			transform.rotation = Quaternion.Euler(0, _lookAngle, 0);

			_tiltAngle -= _smoothY * targetSpeed;
			_tiltAngle = Mathf.Clamp(_tiltAngle, _minAngle, _maxAngle);
			pivot.localRotation = Quaternion.Euler(_tiltAngle, 0, 0);
		}
	}
}
