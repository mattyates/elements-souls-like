﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YesDev.Controller
{
	public class InputHandler : MonoBehaviour
	{
		// Stick Inputs
		protected float _verticalInput;
		protected float _horizontalInput;

		// Button Inputs
		protected bool _X_INPUT;
		protected bool _O_INPUT;
		protected bool _SQR_INPUT;
		protected bool _TRI_INPUT;

		// Trigger & Bumper Inputs
		protected bool _R1_INPUT;
		protected bool _R2_INPUT;
		protected bool _L1_INPUT;
		protected bool _L2_INPUT;

		protected StateManager _states;
		protected CameraManager _camManager;

		protected float _delta;

		private void Start()
		{
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;

			_states = GetComponent<StateManager>();
			_states.Init();

			_camManager = CameraManager.instance;
			_camManager.Init(this.transform);
		}

		private void Update()
		{
			_delta = Time.deltaTime;
			_states.Tick(_delta);
		}

		private void FixedUpdate()
		{
			_delta = Time.fixedDeltaTime;
			GetInput();
			UpdateStates();
			_camManager.Tick(_delta);
		}

		protected void GetInput()
		{
			_verticalInput = Input.GetAxis("Vertical");
			_horizontalInput = Input.GetAxis("Horizontal");

			_O_INPUT = Input.GetButton("CircleBtn");

			_R2_INPUT = Input.GetButton("R2");
			_L2_INPUT = Input.GetButton("L2");

			_R1_INPUT = Input.GetButton("R1");
			_L1_INPUT = Input.GetButton("L1");

			_X_INPUT = Input.GetButton("CrossBtn");
			_O_INPUT = Input.GetButton("CircleBtn");
			_SQR_INPUT = Input.GetButton("SquareBtn");
			_TRI_INPUT = Input.GetButton("TriangleBtn");
		}

		protected void UpdateStates()
		{
			_states.horizontalInput = _horizontalInput;
			_states.verticalInput = _verticalInput;

			Vector3 v = _verticalInput * _camManager.transform.forward;
			Vector3 h = _horizontalInput * _camManager.transform.right;
			_states.moveDir = (v + h).normalized;
			float m = Mathf.Abs(_horizontalInput) + Mathf.Abs(_verticalInput);
			_states.moveAmount = Mathf.Clamp01(m);
			_states.run = _X_INPUT ? (_states.moveAmount > 0) : false;
			_states.r1Input = _R1_INPUT;
			_states.r2Input = _R2_INPUT;
			_states.l1Input = _L1_INPUT;
			_states.l2Input = _L2_INPUT;
			_states.punch = _SQR_INPUT;
			_states.kick = _TRI_INPUT;
			_states.FixedTicket(_delta);
		}

		private void TestForAllKeyCodeInputs()
		{
			System.Array values = System.Enum.GetValues(typeof(KeyCode));
			 foreach(KeyCode code in values){
				 if(Input.GetKeyDown(code)){ Debug.Log(System.Enum.GetName(typeof(KeyCode), code)); }                    
			 }
		}
	}
}
