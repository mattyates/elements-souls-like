﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YesDev.Controller
{
	public class StateManager : MonoBehaviour
	{
		[Header("Init")]
		public GameObject activeInputModel;

		[Header("Inputs")]
		public float verticalInput,
					 horizontalInput;
		
		public float moveAmount;
		public Vector3 moveDir;

		public bool r2Input,
					r1Input,
					l2Input,
					l1Input;


		[Header("Stats")]
		public float moveSpeed = 3.5f;
		public float runSpeed = 7f;
		public float rotateSpeed = 9;
		public float distToGround = 0.5f;

		[Header("States")]
		public bool onGround;
		public bool run;
		public bool punch;
		public bool kick;
		public bool lockOn;
		public bool inAction;
		public bool canMove;

		[HideInInspector]
		public UnityEngine.Animator anim;

		[HideInInspector]
		public Rigidbody rigid;

		[HideInInspector]
		public float delta;

		[HideInInspector]
		public LayerMask ignoreLayers;

		private float _actionDelay;

		public void Init()
		{
			PreVerify();
			SetupAnimator();
			SetupRigidbody();
			SetupLayerMasks();
		}

		protected void PreVerify()
		{
			// Safety checks in case values aren't set
			if (moveSpeed <= 0) moveSpeed = 2;
			if (runSpeed <= 0) runSpeed = 3.5f;
		}

		protected void SetupAnimator()
		{
			// Try to get animator from children if active model is null
			if (activeInputModel == null)
			{
				anim = GetComponentInChildren<UnityEngine.Animator>();
				if (anim == null)
				{
					Debug.LogError("No model found");
				}
			}

			if (anim == null)
				anim = activeInputModel.GetComponent<UnityEngine.Animator>();

			anim.applyRootMotion = false;
		}

		protected void SetupRigidbody()
		{
			rigid = GetComponent<Rigidbody>();
			rigid.angularDrag = 999;
			rigid.drag = 4;
			rigid.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
		}

		protected void SetupLayerMasks()
		{
			gameObject.layer = 9;
			ignoreLayers = ~(1 << 10);;
		}

		public void Tick(float d)
		{
			delta = d;
			onGround = OnGround();
			anim.SetBool("OnGround", onGround);
		}

		public void FixedTicket(float d)
		{
			delta = d;

			DetectAction();
			if (inAction)
			{
				_actionDelay += delta;
				if (_actionDelay > 0.2f)
				{
					inAction = false;
					_actionDelay = 0;
				}
				{
					return;
				}
			}

			canMove = anim.GetBool("CanMove");
			if (!canMove) return;

			rigid.drag = (moveAmount > 0 || !onGround) ? 0 : 4;
			float targetSpeed = run ? runSpeed : moveSpeed;

			if (onGround)
				rigid.velocity = moveDir * (targetSpeed * moveAmount);

			if (run) lockOn = false;

			if (!lockOn)
			{
				Vector3 targetDir = moveDir;
				targetDir.y = 0;
				if (targetDir == Vector3.zero) targetDir = transform.forward;
				Quaternion tr = Quaternion.LookRotation(targetDir);
				Quaternion targetRotation = Quaternion.Slerp(transform.rotation, tr, delta * moveAmount * rotateSpeed);
				transform.rotation = targetRotation;
			}

			HandleMovementAnimations();
		}

		public void DetectAction()
		{
			if (!canMove || (!punch && !kick)) return;

			string targetAnim = null;

			if (punch) targetAnim = "Punch_Right_UpperCut"; // Punch_Right_Straight
			else if (kick) targetAnim = "Kick_Right_Spin_Heel";

			if (!string.IsNullOrEmpty(targetAnim))
			{
				canMove = false;
				inAction = true;
				anim.CrossFade(targetAnim, 0.2f);
				rigid.velocity = Vector3.zero;
			}
		}

		protected void HandleMovementAnimations()
		{
			anim.SetBool("Run", run);
			anim.SetFloat("Vertical", run ? moveAmount : Mathf.Clamp(moveAmount, 0, 0.5f), 0.4f, delta);
		}

		public bool OnGround()
		{
			bool r = false;

			Vector3 origin = transform.position + (Vector3.up * distToGround);
			Vector3 dir = -Vector3.up;
			float dist = distToGround + 0.3f;
			RaycastHit hit;
			if (Physics.Raycast(origin, dir, out hit, dist, ignoreLayers))
			{
				r = true;
				Vector3 targetPos = hit.point;
				transform.position = targetPos;
			}

			return r;
		}
	}
}
